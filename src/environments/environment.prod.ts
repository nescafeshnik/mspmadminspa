export const environment = {
  production: true,
  baseUrl: 'https://mspm.homecredit.ru/api/',
  tokenKey: 'access_token',
};
