import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent, InterviewsComponent, InterviewsSettingsComponent, EvolutionLinkComponent, ChecklistsComponent, EditChecklistsComponent , EditInterviewsComponent} from './shared/components';
import { AuthGuardService } from './shared/services';
import { DxDataGridModule, DxFormModule } from 'devextreme-angular';

const routes: Routes = [
  {
    path: 'login-form',
    component: LoginFormComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: '**',
    redirectTo: 'home',
  },
  {
    path: 'interviews',
    component: InterviewsComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'interviews-settings',
    component: InterviewsSettingsComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'evolution',
    component: EvolutionLinkComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'checklists',
    component: ChecklistsComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'checklists/:id/edit',
    component: EditChecklistsComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'interviews/:id/edit',
    component: EditInterviewsComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'interviews/add',
    component: EditInterviewsComponent,
    canActivate: [ AuthGuardService ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true }), DxDataGridModule, DxFormModule],
  providers: [AuthGuardService],
  exports: [RouterModule],
})
export class AppRoutingModule { }
