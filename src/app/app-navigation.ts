export const navigation = [
  {
    text: 'Эволюция',
    path: '/evolution',
    icon: 'preferences'
  },
  {
    text: 'Чеклисты',
    path: '/checklists',
    icon: 'selectall'
  },
  {
    text: 'Опросы',
    path: '/interviews',
    icon: 'event'
  },
  // {
  //   text: 'Настройки опросов',
  //   path: '/interviews-settings',
  //   icon: 'preferences'
  // },
];
