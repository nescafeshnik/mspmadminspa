import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { SideNavOuterToolbarModule, SideNavInnerToolbarModule, SingleCardModule } from './layouts';
import { FooterModule, LoginFormModule } from './shared/components';
import { AuthService, ScreenService, AppInfoService, AuthHttpService, InterviewsService, InterviewSettingsService } from './shared/services';
import { UnauthenticatedContentModule } from './unauthenticated-content';
import { AppRoutingModule } from './app-routing.module';
import { environment } from '../environments/environment';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { BaseInterceptor } from './shared/services/base-interceptor';
import { InterviewsComponent } from './shared/components/interviews/interviews.component';
import { InterviewsSettingsComponent } from './shared/components/interviews-settings/interviews-settings.component';
import { DxDataGridModule } from 'devextreme-angular';
import { EvolutionLinkComponent } from './shared/components/evolution-link/evolution-link.component';
import { DxRangeSelectorModule, DxButtonModule } from "devextreme-angular";
import { ChecklistsComponent } from './shared/components/checklists/checklists.component';
import { EditChecklistsComponent } from './shared/components';
import { DxFormModule, DxAccordionModule } from 'devextreme-angular';
import { EditInterviewsComponent } from './shared/components/interviews/edit-interviews/edit-interviews.component';
import { FormsModule } from '@angular/forms';
import {
  DxSelectBoxModule,
  DxTextBoxModule,
  DxColorBoxModule,
  DxNumberBoxModule,
  DxSwitchModule,
  DxCheckBoxModule,
  DxValidatorModule,
  DxValidationSummaryModule,
  DxDateBoxModule
} from 'devextreme-angular';


@NgModule({
  declarations: [
    AppComponent,
    InterviewsComponent,
    InterviewsSettingsComponent,
    EvolutionLinkComponent,
    ChecklistsComponent,
    EditChecklistsComponent,
    EditInterviewsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SideNavOuterToolbarModule,
    SideNavInnerToolbarModule,
    SingleCardModule,
    FooterModule,
    LoginFormModule,
    UnauthenticatedContentModule,
    AppRoutingModule,
    DxDataGridModule,
    DxRangeSelectorModule,
    DxButtonModule,
    DxFormModule,
    DxAccordionModule,
    DxSelectBoxModule,
    DxTextBoxModule,
    DxColorBoxModule,
    DxNumberBoxModule,
    DxSwitchModule,
    DxCheckBoxModule,
    DxValidatorModule,
    DxDateBoxModule,
    DxValidationSummaryModule,
    FormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return sessionStorage.getItem(environment.tokenKey);
        },
      },
    }),
  ],
  providers: [
    AuthService, 
    ScreenService, 
    AppInfoService, 
    AuthHttpService,     
    InterviewsService,
    InterviewSettingsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BaseInterceptor,
      multi: true,
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }