import { Component, HostBinding } from '@angular/core';
import { AuthService, ScreenService, AppInfoService, AuthHttpService } from './shared/services';
import { Router} from '@angular/router';
import { formatMessage, loadMessages, locale } from 'devextreme/localization';
import ruMessages from 'devextreme/localization/messages/ru.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  @HostBinding('class') get getClass() {
    return Object.keys(this.screen.sizes).filter(cl => this.screen.sizes[cl]).join(' ');
  }

  constructor(private authService: AuthService, private screen: ScreenService, public appInfo: AppInfoService,  public authHttpService: AuthHttpService, private router: Router) { 
      loadMessages(ruMessages);
      locale("ru");
  }

  isAuthenticated() {
    return this.authService.loggedIn;
  }

  ngOnInit() {
    const logged = this.authService.loggedIn;
    if (!logged){
      this.router.navigate(['/login-form']);
    }
  }
}
