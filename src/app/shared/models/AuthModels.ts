export class AuthRequest {
    userName: string;
    pwd: string;
    timeZone: string;
    clientVersion: string;
    deviceInfo: DeviceInfo
}

export class DeviceInfo {
    name: "Web";
    model: "Web";
    osType: number;
    osVersion: string;
    deviceToken: string;
    udid: string;
}

export class  AuthResponse {
    UserFIO: string;
    UserPhone: string;
    NeedCheckOtp: boolean;
    Bearer: string;
    TokensPair: TokensPair
}

export class  TokensPair {
    AccessToken: string;
    RefreshToken: string;
    RefreshTokenExpiredAt: string;
}

export class AuthResult{
    needCheckOtp: boolean;
    success: boolean
}

export class SendOtpRequest {
    deviceToken: string;
    udid: string;
}

export class CheckOtpRequest {
    deviceToken: string;
    code: string;
    udid: string;
}

export class SendOtpResponse {
    OtpExpiredSeconds: number
    PhoneNumber: string
}


export class User {
    userFio : string;
    userPhone: string;
}

export class RefreshRequest {
    deviceToken: string;
    refreshToken: string;
    timeZone: number
}