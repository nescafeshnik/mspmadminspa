import { Component, OnInit } from '@angular/core';
import { InterviewSettings, InterviewSettingsService } from '../../services/interview-settings.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import notify from 'devextreme/ui/notify';
import * as _ from 'lodash';

@Component({
  selector: 'app-interviews-settings',
  templateUrl: './interviews-settings.component.html',
  styleUrls: ['./interviews-settings.component.scss']
})
export class InterviewsSettingsComponent implements OnInit {

  constructor(private settingsService: InterviewSettingsService, private http: HttpClient) { }

  interviewSettings: InterviewSettings[] = [];

  ngOnInit(): void {
    this.settingsService.getInterviewsSettings().subscribe((res) => {
      this.interviewSettings = res;
    })
  }

  async saveRow(e: any) {
      var a = e;
  }

  rowRemoving(e: any) {
    var res = this.settingsService.deleteSetting(e.key).toPromise()
    .catch((err) => {
        notify("Ошибка при удалении", 'error', 2000);
        return true;
    });
    e.cancel = res;
  }

  rowInsertingOrUpdate(e: any) {
    var data: InterviewSettings;
    if (e.oldData && e.newData){
      data = _.merge(e.oldData, e.newData);
    }else{
      data = e.data;
    }
    var res = this.settingsService.createOrUpdateSetting(data).toPromise()
    .then((resp) => {
      return false;
    })
    .catch((err) => {
        notify("Ошибка при создании", 'error', 2000);
        return true;
    });
    e.cancel = res;
  }

}
