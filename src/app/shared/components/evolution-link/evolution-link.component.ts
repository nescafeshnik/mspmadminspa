import { Component, OnInit, ViewChild } from '@angular/core';
import { EvolutionLinkService, EvolutionLink } from '../../services/evolution-link.service';
import { takeUntil, tap, catchError, finalize } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import notify from 'devextreme/ui/notify';
import { Observable, throwError } from 'rxjs';
import { DxDataGridComponent} from 'devextreme-angular';
import * as _ from 'lodash';

@Component({
  selector: 'app-evolution-link',
  templateUrl: './evolution-link.component.html',
  styleUrls: ['./evolution-link.component.scss']
})
export class EvolutionLinkComponent implements OnInit {

  constructor(private evolutionService: EvolutionLinkService) { }

  @ViewChild(DxDataGridComponent, { static: true }) grid!: DxDataGridComponent;

  items: EvolutionLink[] = [];
  linkPattetn = /^(http|https).*$/

  ngOnInit(): void {
    this.evolutionService.getEvolutionItems().subscribe((res) => {
      this.items = res;
    })
  }

  rowRemoving(e: any) {
    var res = this.evolutionService.deleteEvolution(e.key).toPromise()
    .catch((err) => {
        notify("Ошибка при удалении", 'error', 2000);
        return true;
    });
    e.cancel = res;
  }

  rowInserting(e: any) {
    var res = this.evolutionService.createEvolution(e.data).toPromise()
    .then((resp) => {
        e.data.id = (resp as EvolutionLink).id;
        return false;
    })
    .catch((err) => {
        notify("Ошибка при создании", 'error', 2000);
        return true;
    });
    e.cancel = res;
  }

  onRowPrepared(e: any) {  
    const currentDate = new Date();
    currentDate.setHours(0,0,0,0);
    if (e.rowType == 'data'){
      var selected = this.items.filter(f => new Date(f.validFrom) <= currentDate).sort((x, y) => (x.validFrom <= y.validFrom ? 1 : -1))[0];
      if (selected.id == e.data.id){
        e.rowElement.style.backgroundColor = '#a3ebb1';
      }
      if (new Date(selected.validFrom) > new Date(e.data.validFrom)){
        e.rowElement.style.backgroundColor = '#d3d3d38f';
      }
    }
  }  

  isCurrentLink(id: number){
    var item = this.items.filter(f => f.id === id);
  }

  getUrl(e: any){
    return "<a href='http://google.com'>goo</a>";
  }
}
