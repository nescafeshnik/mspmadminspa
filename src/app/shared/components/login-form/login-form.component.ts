import { CommonModule } from '@angular/common';
import { Component, NgModule } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxButtonModule } from 'devextreme-angular'
import { DxLoadIndicatorModule } from 'devextreme-angular/ui/load-indicator';
import notify from 'devextreme/ui/notify';
import { AuthService } from '../../services';
import { AuthResult, SendOtpResponse } from '../../models/AuthModels';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {
  loading = false;
  otpInput = false;
  message:string | null = null;
  formData: any = {};

  constructor(private authService: AuthService, private router: Router) { }

  async onSubmit(e: Event) {
    e.preventDefault();
    var { login, password, code } = this.formData;
    this.loading = true;

    if (!this.otpInput){
      const result = await this.authService.logIn(login, password);
      if (result.needCheckOtp) {
        await this.sendOtp();
      }
      this.checkAuthResult(result);
      this.loading = false;
    }else{
      const success = await this.checkOtp(code);
      if (success){
        const result = await this.authService.logIn(login, password);
        this.otpInput = false;
        this.checkAuthResult(result);
      }
      this.loading = false;
    }
  }

  private checkAuthResult(result: any){
    if (!result.success){
      notify("Ошибка авторизации. Проверьте логин и пароль.", 'error', 5000);
    }
    else if (!result.needCheckOtp && !result.hasAdminRole){
      notify("У вас нет прав для входа в приложение. Обратитесь к администратору Alexey.Medov@homecredit.ru", 'error', 5000);
      this.otpInput = false;
    }
  }

  private async sendOtp(){
    var otpResult = await this.authService.sendOtp();
    if (!otpResult.success){
      notify("Ошибка при отправке кода подтвреждения.", 'error', 5000);
    }else{
      this.otpInput = true;
    }
  }

  private async checkOtp(code: any){
    const checkOtpResult = await this.authService.checkOtp(code);
    if (!checkOtpResult.success){
      notify("Ошибка при проверке кода подтвреждения, попробуйте еще раз.", 'error', 5000);
      return false;
    }
    return true;
  }

}
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DxFormModule,
    DxLoadIndicatorModule,
    DxButtonModule
  ],
  declarations: [ LoginFormComponent ],
  exports: [ LoginFormComponent ]
})
export class LoginFormModule { }


// P@ssw0rd01