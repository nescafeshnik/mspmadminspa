import { Component, OnInit, ViewChild  } from '@angular/core';
import { ChecklistsService, CheckList, CheckListQuestions, QuestionReorderModel } from '../../../services';
import { ActivatedRoute, Router } from '@angular/router';
import { map, tap, switchMap, takeUntil } from 'rxjs/operators';
import notify from 'devextreme/ui/notify';
import * as _ from 'lodash';
import { DxDataGridComponent} from 'devextreme-angular';

@Component({
  selector: 'app-edit-checklists',
  templateUrl: './edit-checklists.component.html',
  styleUrls: ['./edit-checklists.component.scss']
})
export class EditChecklistsComponent implements OnInit {

      constructor(private checklistsService: ChecklistsService, private readonly route: ActivatedRoute, private readonly router : Router) {
        this.onReorder = this.onReorder.bind(this);
      }

      @ViewChild(DxDataGridComponent, { static: true }) grid!: DxDataGridComponent;

      id: number|null = null;
      checklist: CheckList | null = null;
      questions: CheckListQuestions[] = [];
      lastQuestion : CheckListQuestions | null = null;
      title: string = '';
      ngOnInit(): void {
        this.route.paramMap.subscribe((res) => {
          const id  = parseInt(res.get('id') ?? "0");
          this.checklistsService.getFullCheckList(id).subscribe((res) => {
            this.checklist = res;
            this.questions = res.questions;
            this.title = this.checklist.name;
          })
        })
      }

      rowRemoving(e: any) {
        var res = this.checklistsService.deleteQuestion(e.key).toPromise()
        .catch((err) => {
            notify("Ошибка при удалении", 'error', 2000);
            return true;
        });
        e.cancel = res;
      }

      rowInserting(e: any) {
        if (this.checklist === null){
          return;
        }
        var res = this.checklistsService.createQuestion(this.checklist.id, e.data).toPromise()
        .then((resp) => {
            e.data.id = (resp as CheckListQuestions).id;
            return false;
        })
        .catch((err) => {
            notify("Ошибка при создании", 'error', 2000);
            return true;
        });
        e.cancel = res;
      }

      rowUpdating(e: any) {
        var data = _.merge(e.oldData, e.newData);  
        var res = this.checklistsService.updateQuestion(e.key, data).toPromise()
        .then((resp) => {
            this.lastQuestion = (resp as CheckListQuestions);
            return false;
        })
        .catch((err) => {
            notify("Ошибка при обновлении", 'error', 2000);
            return true;
        });
        e.cancel = res;
      }

      rowUpdated(e: any) {
        if (this.lastQuestion !== null){
           e.data.id = this.lastQuestion.id;
           var index = _.findIndex(this.questions, { 'id': this.lastQuestion.id });
           this.questions[index] = this.lastQuestion;
           this.lastQuestion = null;
           this.grid.instance.beginUpdate();
           this.grid.instance.endUpdate();
        }
      }

      back() {
        this.router.navigate(['/checklists']);
      }

      onEditorPreparing (rowData: any) {
        const question =  _.maxBy(this.questions, 'order');
        if (question !== undefined){
            const maxOrder = question.order + 1;
            rowData.data.order = maxOrder;
        }
      }

      onReorder(e: any) {
          var visibleRows = e.component.getVisibleRows(),
          toIndex = this.questions.indexOf(visibleRows[e.toIndex].data),
          fromIndex = this.questions.indexOf(e.itemData);
          const toIndexOrder = this.questions[toIndex].order;
          this.questions.splice(fromIndex, 1);
          this.questions.splice(toIndex, 0, e.itemData);
          var questionReorderRequest: QuestionReorderModel[] = [];
          var curOrderIndex: number = 1;
          _.forEach(this.questions, function(elem: CheckListQuestions){
            questionReorderRequest.push(new QuestionReorderModel(elem.id, curOrderIndex));
            elem.order = curOrderIndex;
            curOrderIndex++;
          });
          this.checklistsService.questionReorder(questionReorderRequest).toPromise()
          .catch((err) => {
            notify("Ошибка при обновлении", 'error', 2000);
            return true;
        });;
      }
}
