import { Component, OnInit, ViewChild } from '@angular/core';
import { ChecklistsService, CheckList } from '../../services';
import { takeUntil, tap, catchError, finalize } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import notify from 'devextreme/ui/notify';
import { Observable, throwError } from 'rxjs';
import { DxDataGridComponent} from 'devextreme-angular';
import { Router} from '@angular/router';

@Component({
  selector: 'app-checklists',
  templateUrl: './checklists.component.html',
  styleUrls: ['./checklists.component.scss']
})
export class ChecklistsComponent implements OnInit {

  constructor(private checklistsService: ChecklistsService, private router: Router) { }

  checklists: CheckList[] = [];

  ngOnInit(): void {
    this.checklistsService.getCheckLists().subscribe((res) => {
      this.checklists = res;
    })
  }

  onEditingStart(e: any) {
    e.cancel = true;
    this.router.navigate(["/checklists", e.key, "edit"]);
  }

  onRowClick(e: any) {
    e.cancel = true;
    this.router.navigate(["/checklists", e.key, "edit"]);
  }
}
