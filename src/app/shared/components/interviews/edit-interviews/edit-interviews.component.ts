import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { InterviewsService, Interview, Question, EnumDisplay, Answer, InterviewTypeEnum} from '../../../services/interviews.service';
import notify from 'devextreme/ui/notify';
import { DxFormComponent } from 'devextreme-angular';
import { InterviewSettings, InterviewSettingsService } from '../../../services/interview-settings.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-edit-interviews',
  templateUrl: './edit-interviews.component.html',
  styleUrls: ['./edit-interviews.component.scss']
})
export class EditInterviewsComponent implements OnInit {
  @ViewChild('interviewForm') interviewForm : any;
  constructor(private interviewsService: InterviewsService, private readonly route: ActivatedRoute, private readonly router : Router, private settingsService: InterviewSettingsService) { }
  stylingMode = 'outlined';
  labelMode = 'static';
  interview: Interview = new Interview();
  settings: InterviewSettings = new InterviewSettings();
  title: string | null = null;
  InterviewTypeDisplay: EnumDisplay[] = [];
  CommentTypeDisplay: EnumDisplay[] = [];
  QuestionTypeDisplay: EnumDisplay[] = [];
  notificationHour: Date = new Date();
  resultDistributionHour: Date = new Date();
  interviewDate: Date = new Date();

  ngOnInit(): void {
    this.interview = new Interview();
    this.interview.questions = [];
    this.route.paramMap.subscribe((res) => {
      const id  = parseInt(res.get('id') ?? "0");
      if (id > 0){
        this.interviewsService.getInterview(id).subscribe((res) => {
          this.interview = res;
          this.title = res.name;
          this.settingsService.getInterviewsSetting(res.settingsId).subscribe((res) => {
            this.settings = res;
            this.notificationHour = new Date(2000, 0, 0, this.settings.notificationHour);
            this.resultDistributionHour = new Date(2000, 0, 0, this.settings.resultDistributionHour);
            const str = this.settings.cronExpression.split(" ");
            var year = new Date().getFullYear();
            var month = +str[4];
            var day = +str[3];
            this.interviewDate = new Date(year, month - 1, day, 23, 59);
          })
        })        
      }else{
        this.title = "Новый опрос";
        var q = new Question();
        q.answers.push(new Answer());
        q.answers.push(new Answer());
        q.answers.push(new Answer());
        this.interview.questions.push(q);
      }

    })
    this.InterviewTypeDisplay = this.interviewsService.getInterviewTypesDisplay();
    this.CommentTypeDisplay = this.interviewsService.getCommentTypeDisplay();
    this.QuestionTypeDisplay = this.interviewsService.getQuestionTypeDisplay();
  }

  back() {
    this.router.navigate(['/interviews']);
  }

  formError(e: any){
    var a = e;
  }

  addQuestion() {
    var a = this.interviewForm;
    if (this.interview.type === 'Smile' && this.interview.questions){
      notify("Для опроса с типом настроение можно добавить только один вопрос", 'error', 5000);
    }else{
      var q = new Question();
      q.answers.push(new Answer());
      q.answers.push(new Answer());
      q.answers.push(new Answer());
      this.interview.questions.push(q);
    }
  }


  addAnswer(q: Question) {
    if (q.answers === null || q.answers.length == 3){
      notify("Можно добавить максимум 3 ответа", 'error', 5000);
    }else{
      q.answers.push(new Answer());
    }
  }

  saveInterview(e: Event) {
    e.preventDefault();
    this.settingsService.createOrUpdateSetting(this.settings).toPromise()
    .then((resp) => {
        this.interview.settingsId = resp.id;
        _.forEach(this.interview.questions, function(value : Question, key) {
          value.order = key;
          _.forEach(value.answers, function(value : Answer, key1) {
            value.order = key1;
          });
        });
        this.interviewsService.createOrUpdateInterview(this.interview).toPromise()
        .then((resp1) => {
            this.router.navigate(["/interviews"]);
            notify("Информация обновлена", 'success', 5000);
         })
        .catch((err1) => {
            notify("Ошибка при сохранении", 'error', 5000);
            return true;
        });
    })
    .catch((err) => {
        notify("Ошибка при сохранении", 'error', 5000);
        return true;
    });
  }

  changeNotificationHour(e: any){
    this.settings.notificationHour = e.value.getHours();
  }

  changeDistributionHour(e: any){
    this.settings.resultDistributionHour = e.value.getHours();
  }

  changeCronExpression(e: any){
    this.settings.cronExpression = "0 59 23 " +  e.value.getDate() + " " + ( e.value.getMonth() + 1)  +" ? " +  e.value.getFullYear();
  }

  initCronExpression(){
    if (this.settings.cronExpression === undefined){
      var curDate = new Date();
      this.settings.cronExpression = "0 59 23 " + curDate.getDate() + " " + (curDate.getMonth() + 1) +" ? " +  curDate.getFullYear();     
    }

  }
}
