import { Component, OnInit } from '@angular/core';
import { InterviewsService, Interview} from '../../services/interviews.service';
import { dxToolbarOptions, dxToolbarItem } from 'devextreme/ui/toolbar';
import { InterviewSettings, InterviewSettingsService } from '../../services/interview-settings.service';
import { Router} from '@angular/router';
import notify from 'devextreme/ui/notify';
import * as _ from 'lodash';

@Component({
  selector: 'app-interviews',
  templateUrl: './interviews.component.html',
  styleUrls: ['./interviews.component.scss']
})
export class InterviewsComponent implements OnInit {
  constructor(private interviewsService: InterviewsService, private router: Router, private settingsService: InterviewSettingsService) { }

  interviews: Interview[] = [];
  interviewsSettings: InterviewSettings[] = [];

  ngOnInit(): void {
    this.interviewsService.getInterviews().subscribe((res1) => {
      this.settingsService.getInterviewsSettings().subscribe((res) => {
         this.interviews = res1;
         this.interviewsSettings = res;
         _.each(this.interviews, function(e: Interview){
             var setting = _.find(res, function(setting: InterviewSettings) { return setting.id === e.settingsId});
                if (setting !== undefined){
                  const str = setting.cronExpression.split(" ");
                  var year = new Date().getFullYear();
                  var month = +str[4];
                  var day = +str[3];
                  e.interviewDate = new Date(year, month - 1, day, 23, 59);
                }
                return;

         })
      })
    })
  }

  onEditingStart(e: any) {
    e.cancel = true;
    this.router.navigate(["/interviews", e.key, "edit"]);
  }

  onInitNewRow(e: any) {
    e.cancel = true;
    this.router.navigate(["/interviews/add"]);
  }

  rowRemoving(e: any) {
    var res = this.interviewsService.deleteInterview(e.key).toPromise()
    .catch((err) => {
        notify("Ошибка при удалении", 'error', 2000);
        return true;
    });
    e.cancel = res;
  }
}
