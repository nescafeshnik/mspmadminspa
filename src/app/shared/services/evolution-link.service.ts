import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';

export class EvolutionLink{
    id: number;
    link: string;
    validFrom: Date;
}

@Injectable({
  providedIn: 'root'
})


export class EvolutionLinkService {

  constructor(private http: HttpClient) { }

    getEvolutionItems() {
        return this.http.get<EvolutionLink[]>(environment.baseUrl + "/bb/evolutionSettings");
    }

    getInterviewsSetting(id: number) {
        return this.http.get<EvolutionLink>(environment.baseUrl + "/bb/evolutionSettings/" + id);
    }

    createEvolution(item: EvolutionLink) {
        return this.http.post<EvolutionLink>(environment.baseUrl + "/bb/evolutionSettings", item);
    }

    deleteEvolution(id: number) {
        return this.http.delete(environment.baseUrl + "/bb/evolutionSettings/"+id);
    }
}
