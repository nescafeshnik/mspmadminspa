import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { environment } from '@env/environment';
import { Injectable } from '@angular/core';

@Injectable()
export class BaseInterceptor implements HttpInterceptor {
  constructor(private readonly authService: AuthService) {}

  private static checkApiPath(req: HttpRequest<any>): boolean {
    return req.url.startsWith(environment.baseUrl);
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let request = req;

    if (BaseInterceptor.checkApiPath(req)) {
      const authToken = this.authService.token;

      if (authToken) {
        request = req.clone({
          setHeaders: { Authorization: 'Bearer ' + authToken.AccessToken }
        });
      }
    }
    return next.handle(request);
  }

}
