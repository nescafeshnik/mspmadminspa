import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { environment } from '@env/environment';
import { DxRangeSelectorModule } from "devextreme-angular";

export class InterviewSettings{
    constructor() {
       this.notificationHour = 13;
       this.resultDistributionHour = 20;
    }
    id: number;
    notificationHour:number;
    isEnabledNotification:boolean;
    isEnabledResultDistribution:boolean;
    resultDistributionHour:number;
    cronExpression:string;
    recipients:string;
}

@Injectable({
  providedIn: 'root'
})
export class InterviewSettingsService {


  constructor(private http: HttpClient) { }


  getInterviewsSettings() {
     return this.http.get<InterviewSettings[]>(environment.baseUrl + "/bb/web/notificationsettings");
  }

  getInterviewsSetting(id: number) {
     return this.http.get<InterviewSettings>(environment.baseUrl + "/bb/web/notificationsettings/" + id);
  }

  createOrUpdateSetting(setting: InterviewSettings) {
     return this.http.post<InterviewSettings>(environment.baseUrl + "/bb/web/notificationsettings", setting);
  }

  deleteSetting(id: number) {
     return this.http.delete(environment.baseUrl + "/bb/web/notificationsettings/"+id);
  }
}
