import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthRequest, AuthResponse, DeviceInfo } from '../models/AuthModels';
import { Observable} from 'rxjs';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthHttpService {
  url = environment.baseUrl + "/spm/mspm/v2auth/auth"
  constructor(private http: HttpClient) { }

  apiAuthPost(name: string, password: string): Observable<AuthResponse> {
    var deviceInfo = new DeviceInfo();
    deviceInfo.deviceToken = "123";
    var request = new AuthRequest();
    request.deviceInfo = deviceInfo;
    request.clientVersion = "100.0.0";
    request.pwd = password;
    request.userName = name;
    return this.http.post<AuthResponse>(this.url, request)
  }
}
