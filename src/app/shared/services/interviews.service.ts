import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { environment } from '@env/environment';

export class Interview {
    constructor() {
       this.type = 'Smile';
       this.isEnabled = false;
    }
    id: number;
    name: string;
    description: string;
    isEnabled: boolean;
    isAnonymously: boolean;
    type: string;
    recepientType: number;
    settingsId: number;
    interviewDate: Date;
    questions: Question[];
}

export class Question {
    constructor() {
       this.commentType = 'NotRequired';
       this.questionType = 'Single';
       this.answers = [];
    }
    id: number;
    text: string;
    questionType: string;
    commentType: string;
    order: number;
    answers: Answer[];
}

export class Answer {
    constructor() {
       this.isCorrect = false;
    }
    id: number;
    text: string;
    order: number;
    isCorrect: boolean;
}

export enum InterviewTypeEnum {
    Smile = 1,
    Quiz = 2,
    Rating = 3,
}

enum InterviewRecepientTypeEnum {
    NotSet,
    Mrb,
    Agent,
    All,
}

enum QuestionTypeEnum {
    Single = 1,
    Multipart = 2,
    CommentOnly = 3
}

enum CommentTypeEnum {
    NotNeeded,
    Required,
    NotRequired
}

export class EnumDisplay{
    constructor(key: string, displayName: string) {
       this.key = key;
       this.displayName = displayName;
    }
    key: string;
    displayName: string;
}

@Injectable({
  providedIn: 'root'
})

export class InterviewsService {

    constructor(private http: HttpClient) { 

    }

    getInterviews() {
        return this.http.get<Interview[]>(environment.baseUrl + "/bb/web/interviews");
    }

    getInterview(id: number) {
        return this.http.get<Interview>(environment.baseUrl + "/bb/web/interviews/" + id);
    }

    createOrUpdateInterview(item: Interview) {
        return this.http.post<Interview>(environment.baseUrl + "/bb/web/interviews", item);
    }

    deleteInterview(id: number) {
        return this.http.delete(environment.baseUrl + "/bb/web/interviews/"+id);
    }

    getInterviewTypesDisplay(){
        let display: EnumDisplay[] = [];
        display.push(new EnumDisplay('Smile', 'Настроение'));
        display.push(new EnumDisplay('Quiz', 'Опрос'));
        display.push(new EnumDisplay('Rating', 'Оценка'));
        return display;
    }

    getQuestionTypeDisplay(){
        let display: EnumDisplay[] = [];
        display.push(new EnumDisplay('Single', 'Один вариант ответа'));
        display.push(new EnumDisplay('Multipart', 'Несколько вариантов ответа'));
        display.push(new EnumDisplay('CommentOnly', 'Только комментарий'));
        return display;
    }

    getCommentTypeDisplay(){
        let display: EnumDisplay[] = [];
        display.push(new EnumDisplay('NotNeeded', 'Без комментария'));
        display.push(new EnumDisplay('Required', 'Обязательный комментарий'));
        display.push(new EnumDisplay('NotRequired', 'Необязательный комментарий'));
        return display;
    }
}
