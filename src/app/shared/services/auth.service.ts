import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { environment } from '@env/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthHttpService } from '.'
import { map, tap, catchError } from 'rxjs/operators';
import { Observable, throwError, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthRequest, AuthResponse, DeviceInfo, SendOtpRequest, CheckOtpRequest, SendOtpResponse, User, TokensPair, RefreshRequest } from '../models/AuthModels';
const MIN_REFRESH_TIMEOUT = 3 * 60 * 1000; // in miliseconds
import { HttpErrorResponse } from '@angular/common/http';
import * as _ from 'lodash';
import notify from 'devextreme/ui/notify';
import { v4 as uuidv4 } from 'uuid';
import FingerprintJS from "@fingerprintjs/fingerprintjs";


const defaultPath = '/';
export interface IUser {
  userFio: string;
  userPhone?: string
}
@Injectable()
export class AuthService {
  private refresh: NodeJS.Timeout | undefined;
  private _user: IUser | null = null;
  private visitorId: string = "undefined";

  get loggedIn(): boolean {
    if (this.token !== null){
      return !this.jwtHelper.isTokenExpired(this.token.AccessToken);
    }else{
      return false;
    }
  }


  private setVisitorId(): void {
    FingerprintJS.load()
        .then((fp) => fp.get())
        .then((result) => {
            this.visitorId = result.visitorId;
        });
  }

  private _lastAuthenticatedPath: string = defaultPath;
  set lastAuthenticatedPath(value: string) {
    this._lastAuthenticatedPath = value;
  }

  constructor(private router: Router, private readonly jwtHelper: JwtHelperService, private http: HttpClient)  {
    this.setupRefresh(this.token);
    this.setVisitorId();
  }

  async logIn(login: string, password: string) {
    try {
        var hasAdminRole = false;
        var deviceInfo = new DeviceInfo();
        deviceInfo.deviceToken = this.getDeviceToken();
        deviceInfo.osType = 3;
        deviceInfo.udid = this.visitorId;
        var request = new AuthRequest();
        request.deviceInfo = deviceInfo;
        request.clientVersion = "1.0.0";
        request.pwd = password;
        request.userName = login;
        var result = await this.http.post<AuthResponse>(environment.baseUrl + "/v2auth/auth", request).toPromise();
        if (!result.NeedCheckOtp){
          hasAdminRole = this.hasAdminRole(result.TokensPair);
          if (hasAdminRole){
            this.setupToken(result.TokensPair);
            this.router.navigate(['/evolution']);            
          }
        }
        return {
          success: true,
          needCheckOtp: result.NeedCheckOtp,
          hasAdminRole: hasAdminRole
        };
    }
    catch {
      return {
        success: false,
        needCheckOtp: false
      };
    }
    return {
      success: false,
      needCheckOtp: false
    };
  }

  async sendOtp() {
    try {
        var request = new SendOtpRequest();
        request.deviceToken = this.getDeviceToken();
        request.udid = this.visitorId;
        var result = await this.http.post<SendOtpResponse>(environment.baseUrl + "/v2auth/sendOtp", request).toPromise();
        return {
            success: result
        };
    }
    catch {
      return {
        success: false,
      };
    }
    return {
      success: false,
    };
  }

  async checkOtp(otp: string) {
    try {
        var request = new CheckOtpRequest();
        request.deviceToken = this.getDeviceToken();
        request.udid = this.visitorId;
        request.code = otp.toString();
        var result = await this.http.post(environment.baseUrl + "/v2auth/CheckOtp", request).toPromise();
        return {
            success: true
        };
    }
    catch {
      return {
        success: false,
      };
    }
    return {
      success: false,
    };
  }

  private setupToken(token: TokensPair): void {
    sessionStorage.setItem(environment.tokenKey, token.AccessToken);
    sessionStorage.setItem(environment.tokenKey+"_refresh", token.RefreshToken);
    this.setupRefresh(token);
  }

  private setupRefresh(tokenPair: TokensPair | null): void {
    this.clearRefresh();
    if (tokenPair !== null){
      const token = this.jwtHelper.decodeToken(tokenPair.AccessToken);
      if (token && Number.isFinite(token.exp)) {
        const refreshTimeOut = Math.max(
          Math.round((token.exp * 1000 - Date.now()) * 0.8),
          MIN_REFRESH_TIMEOUT
        );

        this.refresh = setTimeout(() => this.refreshToken(tokenPair), refreshTimeOut);
      }      
    }
  }

  hasAdminRole(tokenPair: TokensPair){
    const token = this.jwtHelper.decodeToken(tokenPair.AccessToken);
    if (token !== null){
      const roles = token['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
      if (roles !== null && Array.isArray(roles)){
         const result = _.find(roles, function(o) { return o == 'Administrator' });
         return result !== null && result !== undefined;
      }
    }
    return false;
  }

  private clearRefresh(): void {
    if (this.refresh) {
      clearTimeout(this.refresh);
      this.refresh = undefined;
    }
  }

  private refreshToken(tokenPair: TokensPair): void {
    const request = new RefreshRequest();
    request.deviceToken = "asdfadsf";
    request.refreshToken = tokenPair.RefreshToken;
    this.http.post<TokensPair>(environment.baseUrl + "/v2auth/RefreshToken", request).subscribe((token) => {
      this.setupToken(token);
    },
      (e) => this.logOut()
    );
  }

  async getUser() {
    if (this.token !== null){
      const parsedToken = this.jwtHelper.decodeToken(this.token.AccessToken);
      if (parsedToken && !this.jwtHelper.isTokenExpired(this.token.AccessToken)){
        const user = new User();
        user.userFio = parsedToken.fullName;
        return {
          isOk: true,
          data: user
        };
      }
    }     
    return {
      isOk: false,
      data: null
    };      
  }

  async logOut() {
    this._user = null;
    sessionStorage.removeItem(environment.tokenKey);
    sessionStorage.removeItem(environment.tokenKey+"_refresh");
    this.router.navigate(['/login-form']);
  }

  get token(): TokensPair | null {
    const access = sessionStorage.getItem(environment.tokenKey);
    const refresh = sessionStorage.getItem(environment.tokenKey+"_refresh");
    if (access != null && refresh != null){
      var pair = new TokensPair();
      pair.AccessToken = access;
      pair.RefreshToken = refresh;
      return pair;
    }
    return null;
  }

  getDeviceToken(){
    var token = sessionStorage.getItem("device_token")
    if (token){
      return token.toString();
    }
    token = uuidv4();
    sessionStorage.setItem("device_token", token.toString());
    return token.toString();
    
  }
}

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router, private authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const isLoggedIn = this.authService.loggedIn;
    const isAuthForm = [
      'login-form',
    ].includes(route.routeConfig?.path || defaultPath);

    if (isLoggedIn && isAuthForm) {
      this.authService.lastAuthenticatedPath = defaultPath;
      this.router.navigate([defaultPath]);
      return false;
    }

    if (!isLoggedIn && !isAuthForm) {
      this.router.navigate(['/login-form']);
    }

    if (isLoggedIn) {
      this.authService.lastAuthenticatedPath = route.routeConfig?.path || defaultPath;
    }

    return isLoggedIn || isAuthForm;
  }
}
