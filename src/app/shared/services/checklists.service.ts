import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { environment } from '@env/environment';
import { DxRangeSelectorModule } from "devextreme-angular";

export class CheckList{
    id: number;
    name: string;
    questions: CheckListQuestions[];
}

export class CheckListQuestions{
    id: number;
    text: string;
    order: number;
}

export class CheckListAnswers{
    id: number;
    text: string;
    order: number;
}

export class QuestionReorderModel{
    constructor(id: number, order: number) {
       this.id = id;
       this.order = order;
    }
    id: number;
    order: number;
}

@Injectable({
  providedIn: 'root'
})
export class ChecklistsService {

  constructor(private http: HttpClient) { }

  getCheckLists() {
     return this.http.get<CheckList[]>(environment.baseUrl + "/bb/Checklists");
  }

  getFullCheckList(id: number) {
     return this.http.get<CheckList>(environment.baseUrl + "/bb/Checklists/"+id);
  }

  updateQuestion(id: number, request: CheckListQuestions) {
     return this.http.put(environment.baseUrl + "/bb/Checklists/Questions/" + id, request);
  }

  createQuestion(checkListId: number, request: CheckListQuestions) {
     return this.http.post(environment.baseUrl + "/bb/Checklists/"+ checkListId +"/Questions", request);
  }

  deleteQuestion(id: number) {
     return this.http.delete(environment.baseUrl + "/bb/Checklists/Questions/"+id);
  }

  questionReorder(request: QuestionReorderModel[]) {
     return this.http.put(environment.baseUrl + "/bb/Checklists/Questions/Reorder", request);
  }
}
